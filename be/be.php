<?php
function connectDatabase() {
    $servername = "localhost";
    $username = "root";
    $password = "111111";
    $dbname = "webphp";
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    return $conn;
}

function disconnectDatabase($conn) {
    mysqli_close($conn);
}

function login($username, $password) {
    $conn = connectDatabase();
    $sql = "SELECT * FROM user WHERE username='$username' AND password='$password'";
    $result = mysqli_query($conn, $sql);
    $success = (mysqli_num_rows($result) == 1);
    disconnectDatabase($conn);
    return $success;
}

function profile($username) {
    $conn = connectDatabase();
    $sql = "SELECT id, `role`, email FROM user WHERE username=?";
    $stmt = mysqli_prepare($conn, $sql);
    mysqli_stmt_bind_param($stmt, "s", $username);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_store_result($stmt);
    $success = (mysqli_stmt_num_rows($stmt) == 1);
    
    if ($success) {
        mysqli_stmt_bind_result($stmt, $id, $role, $email);
        mysqli_stmt_fetch($stmt);
        $resultArray = array(
            'id' => $id,
            'role' => $role,
            'email' => $email,
            'username' => $username,
        );
        return $resultArray;
    }

    mysqli_stmt_close($stmt);
    disconnectDatabase($conn);
    return $success;
}

function register($username, $password, $email) {
    $conn = connectDatabase();
    $result = mysqli_query($conn, "SELECT * FROM user WHERE email = '$email'");
    $result2 = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username'");
    if (mysqli_num_rows($result) > 0) {
        $success = false;
    } else if (mysqli_num_rows($result2) > 0){
        $success = false;
    } else {
        $res_id = mysqli_query($conn, "SELECT MAX(id) as max_id FROM user");
        $row = mysqli_fetch_assoc($res_id);
        $max_id = $row["max_id"];

        if ($max_id == null) {
            $max_id = 0;
        }

        $new_id = $max_id + 1;
        $sql = "INSERT INTO user (id, username, password, email) VALUES ('$new_id','$username', '$password', '$email')";
        if (mysqli_query($conn, $sql)) {
            $success = true;
        } else {
            $success = false;
        }
    }
    disconnectDatabase($conn);
    return $success;
}

function addPostToDatabase($title, $date, $path_image, $username) {
    $conn = connectDatabase();

    $res_id = mysqli_query($conn, "SELECT MAX(id) as max_id FROM feed");
    $row = mysqli_fetch_assoc($res_id);
    $max_id = $row["max_id"];

    if ($max_id == null) {
        $max_id = 0;
    }

    $new_id = $max_id + 1;

    $sql = "INSERT INTO feed (id,title, `date`, pathimage, username) VALUES ('$new_id','$title', '$date', '$path_image', '$username')";
    if ($conn->query($sql) === TRUE) {
        disconnectDatabase($conn);
        return true; 
    } else {
        disconnectDatabase($conn);
        return false;
    }
}

function addLikeToDatabase($username, $id_feed) {
    $conn = connectDatabase();

    $checkExistQuery = "SELECT id FROM `like` WHERE id_feed = '$id_feed' AND `username` = '$username'";
    $result = $conn->query($checkExistQuery);

    if ($result->num_rows > 0) {
        $deleteQuery = "DELETE FROM `like` WHERE id_feed = '$id_feed' AND `username` = '$username'";
        if ($conn->query($deleteQuery) === TRUE) {
            disconnectDatabase($conn);
            return true;
        } else {
            disconnectDatabase($conn);
            return false;
        }
    } else {
        $res_id = mysqli_query($conn, "SELECT MAX(id) as max_id FROM `like`");
        $row = mysqli_fetch_assoc($res_id);
        $max_id = $row["max_id"];
    
        if ($max_id == null) {
            $max_id = 0;
        }
    
        $new_id = $max_id + 1;
    
        $sql = "INSERT INTO `like` (id,id_feed, `username`) VALUES ('$new_id','$id_feed', '$username')";
        if ($conn->query($sql) === TRUE) {
            disconnectDatabase($conn);
            return true;
        } else {
            disconnectDatabase($conn);
            return false;
        }
    }
}


function addCommentToDatabase($username, $id_feed, $title, $reply_id, $date) {
    $conn = connectDatabase();
    $res_id = mysqli_query($conn, "SELECT MAX(id) as max_id FROM `comment`");
    $row = mysqli_fetch_assoc($res_id);
    $max_id = $row["max_id"];

    if ($max_id == null) {
        $max_id = 0;
    }

    $new_id = $max_id + 1;

    $sql = "INSERT INTO `comment` (id, id_feed, `username`, title, reply_id, `datetime`) 
            VALUES ('$new_id', '$id_feed', '$username', '$title', '$reply_id', '$date')";
    if ($conn->query($sql) === TRUE) {
        disconnectDatabase($conn);
        return true;
    } else {
        disconnectDatabase($conn);
        return false;
    }
    
}


function getPostFromDatabase() {
    $conn = connectDatabase();

    $posts = array();

    $sql = "SELECT id, title, date, pathimage, username FROM feed ORDER BY date DESC";
    $result = $conn->query($sql);

    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $row['date'] = date('Y-m-d H:i:s', strtotime($row['date']));
            $posts[] = $row;
        }
    }

    $conn->close();

    return $posts;
}

?>

<?php
$request_uri = $_SERVER['REQUEST_URI'];
$uri_parts = explode('/', $request_uri);
$uri_action = end($uri_parts);



function getAllSessionIDs() {
    $sessionIDs = [];
    foreach ($_SESSION as $key => $value) {
        $sessionID = session_id();
        
        if (!in_array($sessionID, $sessionIDs)) {
            $sessionIDs[] = $sessionID;
        }
    }
    
    return $sessionIDs;
}

function checkss() {
    session_start();
    $cookieHeader = $_SERVER['HTTP_COOKIE'] ?? '';
    if (!empty($cookieHeader)) {
        $cookies = explode(';', $cookieHeader);
        
        foreach ($cookies as $cookie) {
            $cookiePair = explode('=', trim($cookie));
            
            if (count($cookiePair) === 2) {
                $cookieName = $cookiePair[0];
                $cookieValue = $cookiePair[1];
                
                if ($cookieName === 'PHPSESSID') {
                    $currentSessionIDs = getAllSessionIDs();
                    if (empty($currentSessionIDs)){
                        $cke =  false;
                    }
                    else {
                        if ($currentSessionIDs[0] == $cookieValue){
                            
                            return true;
                        }
                        else{
                            return false;
                        }
                    }
                }
            }
        }
    }
    return false;
}

switch ($uri_action) {
    case 'login':
        session_start();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $success = login($username, $password);

            if ($success) {
                session_regenerate_id(true);
                $_SESSION['username'] = $username;
                $_SESSION['timeout'] = time() + (60 * 60 * 24);
                $data = array('result' => $success, 'session_id' => session_id());
            } else {
                $data = array('result' => $success);
            }

            header('Content-Type: application/json');
            echo json_encode($data);
            exit;
        }
        break;
    case 'register':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            $success = register($username, $password, $email);
            $data = array('result' => $success);
            header('Content-Type: application/json');
            echo json_encode($data);
            exit;
        }
        break;
    case 'logout':
        session_start();
        $_SESSION = array();
        session_destroy();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $cookieHeader = $_SERVER['HTTP_COOKIE'] ?? '';

            if (!empty($cookieHeader)) {
                $cookies = explode(';', $cookieHeader);
                
                foreach ($cookies as $cookie) {
                    $cookiePair = explode('=', trim($cookie));
                    
                    if (count($cookiePair) === 2) {
                        $cookieName = $cookiePair[0];
                        $cookieValue = $cookiePair[1];
                        
                        if ($cookieName === 'PHPSESSID') {
                            $currentSessionIDs = getAllSessionIDs();
                            if (empty($currentSessionIDs)){

                                $cke =  true;
                            }
                            else {
                                $cke =false;
                            }
                        }
                    }
                }
            }

            

            header('Content-Type: application/json');
            echo json_encode(['success' => $cke]);
            exit;
                
        }
        break;
    case 'profile':

        $cke = checkss();

        if ($cke){
            header('Content-Type: application/json');
            $username = $_SESSION['username'];
            $result = profile($username);
            echo json_encode(['success' => $result]);
        }
        else{
            header('Content-Type: application/json');
            echo json_encode(['success' => $cke]);
        }
        exit;
    case 'post_article':
        $cke = checkss();

        if ($cke){
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $title = $_POST["title"];
                $uploadDir = "D:/E/thuctap/web/sql/article/";
                $uploadedFile = $uploadDir . basename($_FILES["image"]["name"]);

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $uploadedFile)) {

                    $path_image = $uploadedFile;
                    $username = $_SESSION['username'];
                    $date = date("Y-m-d H:i:s");
                    $cke = addPostToDatabase($title, $date, $path_image, $username);
                    
                    header('Content-Type: application/json');
                    echo json_encode(['success' => $cke]);
                } else {
                    header('Content-Type: application/json');
                    echo json_encode(['success' => $cke, 'error' => 'fail to load image']);
                }

            } else {
                header('Content-Type: application/json');
                echo json_encode(['success' => $cke, 'error' => 'invalid data']);
            }
        }
        else{
            header('Content-Type: application/json');
            echo json_encode(['success' => $cke]);
        }
        exit;
    case 'get_feed':
            $cke = checkss();
    
            if ($cke){
                if ($_SERVER["REQUEST_METHOD"] == "GET") {
                    $cke = getPostFromDatabase();
                    foreach ($cke as &$post) {
                        $imagePath = $post['pathimage'];
                        if (file_exists($imagePath)) {
                            $imageContent = file_get_contents($imagePath);
                            $base64Image = base64_encode($imageContent);
                            $post['pathimage'] = $base64Image;
                        } else {
                            $post['pathimage'] = null;
                        }
                    }
                    
                    header('Content-Type: application/json');
                    echo json_encode(['success' => true, 'data' => $cke]);
                } else {
                    header('Content-Type: application/json');
                    echo json_encode(['success' => $cke, 'error' => 'invalid data']);
                }
            }
            else{
                header('Content-Type: application/json');
                echo json_encode(['success' => $cke]);
            }
            exit;

    case 'like':
        $cke = checkss();

        if ($cke){
            $username = $_SESSION['username'];
            $id_feed = $_POST['postId'];
            $cke = addLikeToDatabase($username, $id_feed);
            header('Content-Type: application/json');
            echo json_encode(['success' => $cke]);
        }
        else{
            header('Content-Type: application/json');
            echo json_encode(['success' => $cke]);
        }
        exit;
    case 'comment':
            $cke = checkss();
    
            if ($cke){
                $username = $_SESSION['username'];
                $id_feed = $_POST['postId'];
                $title = $_POST['title'];
                $reply_id = $_POST['replyid'];
                $date = date("Y-m-d H:i:s");

                $cke = addCommentToDatabase($username, $id_feed, $title, $reply_id, $date);
                header('Content-Type: application/json');
                echo json_encode(['success' => $cke]);
            }
            else{
                header('Content-Type: application/json');
                echo json_encode(['success' => $cke]);
            }
            exit;
    default:
    $data = array('result' => false);
    header('Content-Type: application/json');
    echo json_encode($data);
    break;
}
?>

