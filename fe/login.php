<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#btn-login").click(function() {
                var username = $("#username").val();
                var password = $("#password").val();
                $.ajax({
                    url: "http://localhost:3000/thuctap/web/be/be.php/login",
                    type: "POST",
                    data: {
                        username: username,
                        password: password,
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.result === true && data.session_id !== null) {
                            document.cookie = "session_id=" + encodeURIComponent(data.session_id);
                            window.location.href = "profile.php";
                        } else {
                            alert("Username or password invalid!");
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            });
        });
    </script>
</head>
<body>
    <h1>Login</h1>
    <form>
        <label for="username">Username:</label>
        <input type="text" id="username" name="username" required><br>

        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required><br>

        <button type="button" id="btn-login">Login</button>
    </form>
</body>
</html>
