<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Feed</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <style>
        .post {
            margin-bottom: 20px;
            padding: 10px;
            border: 1px solid #ccc;
        }
        .post {
        margin: 20px; 
        margin-left: 400px;
        margin-right: 400px;
        padding: 10px;
        border: 1px solid #ddd;
        }

        .post img {
            max-width: 100%;
            height: auto; 
            display: block; 
            margin: 10px auto;
        }
    </style>
</head>
<body>
    <h2>New Feed</h2>
    <div id="feedContainer"></div>

    <script>
        function loadFeed() {
            $.ajax({
                url: "http://localhost:3000/thuctap/web/be/be.php/get_feed",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $("#feedContainer").empty();
                    for (var i = 0; i < data.data.length; i++) {
                        var postId = data.data[i].id;

                        var postHtml = "<div class='post' data-post-id='" + postId + "'>" +
                                        "<h3>" + data.data[i].title + "</h3>" +
                                        "<p>Posted by " + data.data[i].username + " on " + data.data[i].date + "</p>" +
                                        "<img src='data:image/jpeg;base64," + data.data[i].pathimage + "' alt='Post Image' style='max-width: 400px; max-height: 500px;'>" +
                                        "<div class='interaction'>" +
                                        "<div><button onclick='likePost(" + postId + ")'>Like</button></div>" +
                                        "<div><input type='text' id='commentInput" + postId + "' placeholder='Enter your comment'>" +
                                        "<button onclick='postComment(" + postId + ")'>Comment</button></div>" +
                                        "</div>" +
                                        "</div>";

                        $("#feedContainer").append(postHtml);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function postComment(postId) {
            var commentInput = $("#commentInput" + postId).val();
            $.ajax({
                url: "http://localhost:3000/thuctap/web/be/be.php/comment",
                type: "POST",
                data: { postId: postId, title: commentInput, replyid : 0 },
                dataType: "json",
                success: function(data) {
                    alert(data.success);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function likePost(postId) {
            $.ajax({
                url: "http://localhost:3000/thuctap/web/be/be.php/like",
                type: "POST",
                data: { postId: postId },
                dataType: "json",
                success: function(data) {
                    alert(data.success);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

    $(document).ready(function() {
            loadFeed();
        });
    </script>
</body>
</html>
