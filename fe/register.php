<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#btn-register").click(function() {
                var username = $("#username").val();
                var password = $("#password").val();
                var email = $("#email").val();
                $.ajax({
                    url: "http://localhost:3000/thuctap/web/be/be.php/register",
                    type: "POST",
                    data: {
                        username: username,
                        password: password,
                        email : email,
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.result == true) {
                            alert("Success!");
                        } else {
                            alert("Username or email invlaid!");
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            });
        });
    </script>
</head>
<body>
    <h1>Register</h1>
    <form>
        <label for="username">Username:</label>
        <input type="text" id="username" name="username" required><br>

        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br>

        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required><br>

        <button type="button" id="btn-register">Register</button>
    </form>
</body>
</html>