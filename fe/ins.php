<!DOCTYPE html>
<html>
<head>
    <title>Feed</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        function submitPost() {
            var title = $("#postTitle").val();
            var formData = new FormData($("#postForm")[0]);

            $.ajax({
                url: "http://localhost:3000/thuctap/web/be/be.php/post_article",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                window.location.href = "ins.php";
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
            });
        }
    </script>
</head>
<button type="button" onclick="window.location.href = 'profile.php';">My profile</button>
<body>
    <h2>Create post!</h2>
    <style>
    #postForm label, #postForm input, #postForm button {
        display: inline-block;
        margin-bottom: 10px;
    }

    #postForm label {
        width: 80px; 
        text-align: right; 
        margin-right: 10px; 
    }

    #postForm input, #postForm button {
        width: 200px; 
    }
    </style>
    <form id="postForm" enctype="multipart/form-data">
        <label for="postTitle">Title :</label>
        <input type="text" id="postTitle" name="title" required>
        
        <label for="postImage">Image :</label>
        <input type="file" id="postImage" name="image" accept="image/*">
        
        <button type="button" onclick="submitPost()">Submit</button>
    </form>
</body>
</html>
<?php include('newfeed.php'); ?>
