<!DOCTYPE html>
<html>
<head>
    <title>Profile</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        var session_id = getCookie('session_id');
        $(document).ready(function() {
            if (session_id) {
                $.ajax({
                    url: "http://localhost:3000/thuctap/web/be/be.php/profile",
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $("#profileInfo").html(
                            "Username: " + data.success.username + "<br>" +
                            "Id: " + data.success.id + "<br>" +
                            "Role: " + data.success.role + "<br>" +
                            "Email: " + data.success.email
                        );
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            } else {
                window.location.href = "login.php";
            }

        });

        function getCookie(name) {
            var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
            if (match) return match[2];
        }

        function logout() {
            $.ajax({
                url: "http://localhost:3000/thuctap/web/be/be.php/logout",
                type: "POST",
                dataType: "json",
                success: function(data) {
                    document.cookie = "session_id=;";
                    window.location.href = "login.php";
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function goToPostPage() {
            window.location.href = "ins.php";
        }

    </script>
<body>
    <h1>Profile</h1>
    <div id="profileInfo"></div>
    <button type="button" onclick="goToPostPage()">New Feed</button>
    <button type="button" onclick="logout()">Logout</button>
</body>
</html>
